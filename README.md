# README #

Module 7

Photo Sharing Site Rubric (also attached as a txt file):

1. Correctly configure drupal with SQL, phpMyAdmin and MAMP onto your EC2 Instance  (15 points)
2. People can register for the site (2pts)
3. Registered users can login to the site (2pts)
4. Registered users can log out of the site (2pts)
5. Registered users can delete their account (but any information they posted while an active user remains, but is changed to anonymous) (4pts)
6. Create a photosharing feed (50pts)
    a. users can post photos with a caption or body of text (20 pts)
    b. users can comment on photos (10pts)
    c. users can delete posts, and all information associated with them (including other users’ comments on their photos) (20 pts)
7. Site is visually appealing and easy to use (10 pts)
8. Creative Portion (10pts)

Link: http://ec2-107-23-218-4.compute-1.amazonaws.com/drupal-8.3.0/